import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from "../pages/Landing";
import Profile from "../pages/profile";
import NotFound from "../pages/NotFound";
import AxiosData from "../pages/axios";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Landing />
        </Route>
        <Route path="/profile" exact>
          <Profile />
        </Route>
        <Route path="/axios" exact>
          <AxiosData />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
