import Routes from "./routes/router";
import "react-responsive-carousel/lib/styles/carousel.min.css";

function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
