import React from "react";

function Footer({ ...props }) {
  const { angka, str, arr, objek, plus } = props;
  return (
    <div className="navbar-container">
      <h1>Footer</h1>
      <h1>{angka}</h1>
      <p>{str}</p>
      <p>{arr[3]}</p>
      <p>{objek.nama}</p>
      <p>{objek.hobby}</p>
      <p>{plus(1,2)}</p>
    </div>
  );
}

export default Footer;
