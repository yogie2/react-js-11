import React, { useState, useEffect } from "react";
import "./card.css";
import axios from "axios";

function AxiosCard({ ...props }) {
  const { axiosData, str, data } = props;
  // console.log("str yang terlempar", str);

  // state untuk menyimpan hasil dari input

  const [inputs, setInputs] = useState({
    title: "",
    body: "",
    userId: null,
  });
  const [save, setSave] = useState([]);
  const [id, setId] = useState(1);

  const eventInput = (event) => {
    setInputs({
      ...inputs,
      [event.target.name]: event.target.value,
    });
  };

  const setDone = (task) => {
    const select = save.find((selected) => selected.id === task.id);
    const notSelected = save.filter((noSelected) => noSelected.id !== task.id);
    select.done = !select.done;
    const newList = [...notSelected, select].sort(
      (min, max) => min.id - max.id
    );
    console.log(newList);
    setSave(newList);
  };
  //perubahan

  const deleteItem = (item) => {
    const deleteTask = save.filter((selected) => selected.id !== item.id);
    setSave(deleteTask);
  };
  const evenClick = () => {
    console.log("data terkirim");
    setId(id + 1);
    setSave([
      ...save,
      { body: inputs.body, title: inputs.title, userId: inputs.userId },
    ]);
  };

  // contoh cara mengambil token yang didapat dari API
  // const token = localStorage.getItem("token");

  
// fungsi post data
  const postData = (data) => {
    axios
      .post("https://jsonplaceholder.typicode.com/posts", data)
      .then((res) => console.log(res));
  };

  const directPost = (data) => {
    axios
      .post("https://jsonplaceholder.typicode.com/posts", data)
      .then((res) =>
        setSave([
          ...save,
          {
            body: res.data.body,
            title: res.data.title,
            userId: res.data.userId,
          },
        ])
      )
      .catch((error) => console.log(error));
    // .then((res) => console.log(res));
  };

  // example post with header

  // const postDataWithHeader = (data) => {
  //   axios
  //     .post("https://jsonplaceholder.typicode.com/posts", data, {
  //       header: {
  //         "Content-type": "application/json; charset=UTF-8",
  //         "Authorization": token,
  //         "api-key": "xxxxyyyzzz",
  //       },
  //     })
  //     .then((res) => console.log(res));
  // };


  // fungsi login
  // const login = (data) => {
  //   axios.post("url api buat login", data).then((res) =>
  //     localStorage.setItem(
  //       "namanya token",
  //       // isinya dari mana?
  //       // contohnya:
  //       res.data.token
  //     )
  //   );
  // };


  // fungsi logout
  // const logout = () => {
  //   window.location.href = "/landingpage";
  //   localStorage.clear()
  // };

  return (
    <div>
      <h1>{data}</h1>
      {/* {axiosData.map((data, index) => {
        return (
          <>
            <div className="axios-map" key={index}>
              <p>{index + 1}.</p>
              <p> email: {data.email}-</p>
              <p>username:{data.username}</p>
            </div>
          </>
        );
      })} */}
      <input
        type="text"
        placeholder="title"
        onChange={eventInput}
        name="title"
      />
      <input type="text" placeholder="body" onChange={eventInput} name="body" />
      <input
        type="text"
        placeholder="userId"
        onChange={eventInput}
        name="userId"
      />
      {/* <input type="text" placeholder="kota" onChange={eventInput} name="kota" /> */}
      {/* <button onClick={evenClick}>Submit</button> */}
      <button onClick={() => directPost()}>Direct Post</button>
      <button onClick={() => postData(save)}>Post</button>
      {/* {save?.map((item) => {
        return (
          <div>
            <p className={item.done ? "todo-list-done" : "todo-list"}>
              {item.name}
            </p>
            <button onClick={() => setDone(item)}>Done</button>
            <button onClick={() => deleteItem(item)}>Delete</button>
          </div>
        );
      })} */}
      {save.map((item) => {
        return (
          <div>
            <p>{item.title}</p>
            <p>{item.body}</p>
            <p>{item.userId}</p>
          </div>
        );
      })}
    </div>
  );
}

export default AxiosCard;
