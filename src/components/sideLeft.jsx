import React from "react";

function SideLeft() {
  return (
    <div className="navbar-container">
      <h1>Side Left</h1>
      <a href="/profile">
        <p>Profile</p>
      </a>
      <a href="/axios">
        <p>Axios</p>
      </a>
    </div>
  );
}

export default SideLeft;
