import React from "react";
import Navbar from "../components/navbar";
import SideLeft from "../components/sideLeft";
import SideRight from "../components/sideRight";
import "../main.css";

const Landing = () => {
  return (
    <>
      <Navbar />
      <div className="main-container">
        <SideLeft />
        {/* center */}
        <div className="main-center">
          <h1>Test React</h1>
        </div>
        {/* center */}
        <SideRight />
      </div>
    </>
  );
};

export default Landing;
