import React, { useState, useEffect, useReducer } from "react";
import axios from "axios";
import AxiosCard from "../components/axiosCard";
// import "./axios.css";

function AxiosData() {
  const [axiosData, setAxiosData] = useState([]);
  const [getData, setGetData] = useState(0);

  // state untuk menyimpan hasil dari input
  const [input, setInput] = useState([
    {
      title: "",
      body: "",
      userId: "",
    },
  ]);
  const [postData, setPostData] = useState([]);

  const initialState = { count: 0, bool: false, user: [] };

  const actionReducer = {
    increase: "increase",
    decrease: "decrease",
    bool: "bool",
    user: "user",
  };

  function reducer(state, action) {
    switch (action.type) {
      case actionReducer.increase:
        return { ...state, count: state.count + 1 };
      case actionReducer.decrease:
        return { ...state, count: state.count - 1 };
      case actionReducer.bool:
        return {
          ...state,
          bool: !state.bool,
        };
      case actionReducer.user:
        return {
          ...state,
          user: action.payload,
        };
      default:
        return "error";
    }
  }

  const onInput = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const onSave = () => {
    setPostData(...postData, {
      title: input.title,
      body: input.body,
      userId: input.userId,
    });
  };

  console.log(input);
  console.log(postData);

  const [states, dispatch] = useReducer(reducer, initialState);

  const didUpdate = () => {
    setGetData(getData + 1);
  };

  const reducerGet = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((red) => dispatch({ payload: red.data, type: "user" }));
  };

  const axiosGet = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((see) => setAxiosData(see.data));
  };

  // console.log("get data", getData);
  // console.log("states", states);

  // console.log("data", axiosData);

  //did mount
  useEffect(() => {
    axiosGet();
    reducerGet();
    console.log("mounting");
  }, []);
  useEffect(() => {
    console.log("ada update");
    return () => {
      axiosGet();
    };
  }, [getData]);
  return (
    <>
      <div className="axios-container">
       
        <button onClick={didUpdate}>Get</button>
      <AxiosCard axiosData={axiosData} str="string" data={getData} />
      {/* Number: {states.count}
      Boolean: {states.bool ? "True" : "False"}
      <button onClick={() => dispatch({ type: "increase" })}>+</button>
      <button onClick={() => dispatch({ type: "decrease" })}>-</button>
      <button onClick={() => dispatch({ type: "bool" })}>Boolean</button> */}
      </div>
    </>
  );
}

export default AxiosData;
