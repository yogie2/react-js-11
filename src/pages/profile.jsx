import React, { useState, useEffect } from "react";
import "./profile.css";
import Footer from "../components/footer";
import {
  Button,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Carousel,
  CarouselItem,
  CarouselIndicators,
  CarouselControl,
} from "reactstrap";
import img1 from "./assets/react.png";
import img2 from "./assets/js.jpeg";
import { Carousel as Carousels } from "react-responsive-carousel";
import axios from "axios";
// import "react-responsive-carousel/lib/styles/carousel.min.css";

function Profile() {
  const [data, setData] = useState([]);
  const [axiosData, setAxiosData] = useState([]);
  const [dark, setDark] = useState(false);
  const [count, setCount] = useState(0);
  const [modal, setModal] = useState(false);
  const trigger = () => setModal(!modal);
  console.log(dark);
  const apiGet = () => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((save) => setData(save));
  };

  const axiosGet = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((see) => setAxiosData(see.data));
  };

  console.log("data dari fetch axios", axiosData);

  const plus = (num1, num2) => {
    return num1 + num2;
  };
  useEffect(() => {
    apiGet(); //invoke
    axiosGet();
  }, []); // menandakan saat page ini di load akan melakukan fungsi di atas
  console.log("ini isi state data", data);

  const slides = [
    {
      src: img1,
      altTest: "Slide 1",
    },
    {
      src: img2,
      altTest: "Slide 2",
    },
  ];

  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === slides.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? slides.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  const slidesItem = slides.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altTest} />
      </CarouselItem>
    );
  });

  return (
    <>
      <div className={dark ? "profile-container-dark" : "profile-container"}>
        <h1>Profile</h1>
        <p>Name: Yogie</p>
        <p>City: Bandung</p>
        <p>Tech Stack: Front End</p>

        <button onClick={() => setDark(!dark)}>Dark</button>
        <h1>{count}</h1>
        <div className="count-flex">
          <button onClick={() => setCount(count + 1)}>+</button>
          <button onClick={() => setCount(count - 1)}>-</button>
        </div>
        <Button color="primary" onClick={trigger}>
          REACTSTRAP BUTTON
        </Button>
        <Row>
          <Col>Col 1</Col>
          <Col>Col 2</Col>
          <Col>Col 3</Col>
        </Row>
        <Footer
          angka={count}
          str={"aku props yang terlempar"}
          arr={[1, 2, 3, 4, 5]}
          objek={{ nama: "yogie", hobby: "tidur" }}
          plus={plus}
        />
      </div>

      <Modal isOpen={modal} toggle={trigger}>
        <img src={img1} alt="img" />
        {axiosData.map((data, index) => {
          return (
            <div key={index}>
              <p>{data.email}</p>
              <p>{data.username}</p>
            </div>
          );
        })}
      </Modal>
      {/* <Carousel activeIndex={activeIndex} next={next} previous={previous}>
        <CarouselIndicators
          items={slides}
          activeIndex={activeIndex}
          onClickHandler={goToIndex}
        />
        {slidesItem}
        <CarouselControl
          direction="prev"
          directionText="Previous"
          onClickHandler={previous}
          className="car-control-prev"
        />
        <CarouselControl
          direction="next"
          directionText="Next"
          onClickHandler={next}
        />
      </Carousel> */}
      {/* <div>
        <Carousels
          showArrows
          showThumbs
          showIndicators
          showStatus
          className="carousels-containers"
        >
          <div className="carousels-img">
            <img src={img1} alt="1" />
            <p>React</p>
          </div>
          <div className="carousels-img">
            <img src={img2} alt="2" />
            <p>JS</p>
          </div>
        </Carousels>
      </div> */}
    </>
  );
}

export default Profile;
